
import string
print("aa")


def mots_list(pFile):
    """ajoute une liste des mots pour la fichier"""
    mots = []  # la liste des mots
    for line in pFile:
        line_list = line.split()
        for mot in line_list:
            mot = mot.lower()
            mot = mot.strip(".,") #sepqrer les mots
            mots.append(mot)
    return mots


def add_mots(mot, mot_dict):
    """ajoute les mots dans une dictionnaire"""
    if mot in mot_dict:
        mot_dict[mot] += 1
    else:
        mot_dict[mot] = 1


def procesus_line(line, dict):
    """traitement des donnees """
    line = line.strip()
    mots_list = line.split()
    for mot in mots_list:
        mot = mot.lower().strip(string.punctuation)  # supprimer les points a la fin de chaqun phrase
        add_mots(mot, dict)  # utiliser la diction


def print_result(mots_dict):
    """formuler les donnees """
    val_key_liste = []
    for key, val in mots_dict.items():
        val_key_liste.append((val, key))
    val_key_liste.sort(reverse=True)  # changer l'or    dre des donnee
    print("%-10s %-10s" % ("word", "count"))
    print("_" * 25)
    for val, key in val_key_liste:
        print("%-12s   %3d" % (key, val))


def main():
    """Le main"""
    words_dict = {}
    print("q")
    pFile = open(r"LES MISERABLES.txt", "r", encoding="utf-8")

    for line in pFile:
        procesus_line(line, words_dict)
    print("the length of the dictionary:", len(words_dict))


main()

